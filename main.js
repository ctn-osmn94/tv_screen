fetch("/text.json")
  .then(response => response.json())
  .then(function(live) {
  liveText(live)
})
function liveText(live) {
  for (let i = 0; i < live.length; i++) {
    window["text"+(i+1)] = document.getElementById("text-"+(i+1))
    eval("text"+(i+1)).innerHTML = live[i].title
  } 
}

let cityName = document.getElementById("city-name")
let cityTemp = document.getElementById("cities-temp")
let country = 'İstanbul'
const apiKey = 'f334e680317adedba5618407a8b68472' 
let a = 0
async function weatherDataFetch() {
await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${country}&appid=${apiKey}&units=metric&lang=tr`)
  .then(response=>response.json())
  .then((data) => {
      console.log(data);
    })
    .catch(error => {
      throw(error);
  })
}

const SECONDS = 1000 * 60 
let counter = 0
setInterval(() => {
  if(counter % 2 == 0) {
    country = 'İstanbul'
    weatherDataFetch()
  } else {
    country = 'Delaware'
    weatherDataFetch()
  }
  counter++
}, SECONDS);

  let coinArray = ["BTC", "ETH", "BNB", "UBXS"] 
  async function coinFetch() {
        await fetch("/coin.json")
          .then(response=>response.json())
          .then((data) => {
            for(let symbol of coinArray) {
              window["price"+(symbol)] = document.getElementById("price-"+(symbol.toLowerCase()))
              window["iconUp"+(symbol)] = document.getElementById("iconup-"+(symbol.toLowerCase()))
              window["iconDown"+(symbol)] = document.getElementById("icondown-"+(symbol.toLowerCase()))
              if (eval(`data.data.${symbol}.quote.USD.percent_change_1h`)>0) {
                eval("price"+(symbol)).innerHTML = eval(`data.data.${symbol}.quote.USD.price.toFixed(1)`)
                eval("price"+(symbol)).style.color = "green"
                eval("iconUp"+(symbol)).style.display = "block"
              } else{
                eval("price"+(symbol)).innerHTML = eval(`data.data.${symbol}.quote.USD.price.toFixed(1)`)
                eval("price"+(symbol)).style.color = "red"
                eval("iconDown"+(symbol)).style.display = "block"
              }
            }
      })
      .catch(error => {
        console.log(error);
        })
      }
    coinFetch()
    setInterval(coinFetch, 360000) 
    
    let videoLink = document.getElementById("video-link")
    let displayVideo = document.getElementById("display-video")
    let bgİmageContainer = document.getElementById("bg-conteiner")
    videoLink.addEventListener("click", () => {
      videoLink.style.display = "none"
      displayVideo.style.display = "block"
    })

    let setHour = document.getElementById("set-hour")
    let hour = new Date().getHours()
    let minutes = new Date().getMinutes()
    setHour.innerHTML = hour+":"+minutes